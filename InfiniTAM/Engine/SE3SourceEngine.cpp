// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "SE3SourceEngine.h"

#include "../Utils/FileUtils.h"

#include <stdio.h>

using namespace InfiniTAM::Engine;

Matrix3f inline rotationFromQuaternion(Vector4f q) {
	Matrix3f rotation;
	rotation.setIdentity();
	float n = dot(q, q);
	if (n < 1e-9) {
		return rotation;
	}
	q *= sqrt(2.0 / n);
	Matrix4f Q = outer(q, q);
	Q = Q.t();
	rotation(0,0) = 1.0 - Q(2,2) - Q(3,3);
	rotation(0,1) = Q(1,2) - Q(3,0);
	rotation(0,2) = Q(1,3) + Q(2,0);
	rotation(1,0) = Q(1,2) + Q(3,0);
	rotation(1,1) = 1.0 - Q(1,1) - Q(3,3);
	rotation(1,2) = Q(2,3) - Q(1,0);
	rotation(2,0) = Q(1,3) - Q(2,0);
	rotation(2,1) = Q(2,3) + Q(1,0);
	rotation(2,2) = 1.0 - Q(1,1) - Q(2,2);

	return rotation.t();
}

 //    q = numpy.array(quaternion, dtype=numpy.float64, copy=True)
 //    n = numpy.dot(q, q)
 //    if n < _EPS:
 //        return numpy.identity(4)
 //    q *= math.sqrt(2.0 / n)
 //    q = numpy.outer(q, q)
 //    return numpy.array([
 //        [1.0-q[2, 2]-q[3, 3],     q[1, 2]-q[3, 0],     q[1, 3]+q[2, 0], 0.0],
 //        [    q[1, 2]+q[3, 0], 1.0-q[1, 1]-q[3, 3],     q[2, 3]-q[1, 0], 0.0],
 //        [    q[1, 3]-q[2, 0],     q[2, 3]+q[1, 0], 1.0-q[1, 1]-q[2, 2], 0.0],
 //        [                0.0,                 0.0,                 0.0, 1.0]])


SE3SourceEngine::SE3SourceEngine(const char *poseMask, const char *inputFileType)
{

	strncpy(this->poseMask, poseMask, BUF_SIZE);

	currentFrameNo = 0;
	cachedFrameNo = -1;
	this->inputFileType = inputFileType;

	cached_pose = NULL;
}

void SE3SourceEngine::loadSE3IntoCache(void)
{
	char str[2048]; FILE *f; bool success = false;

	cached_pose = new ITMSE3Measurement();

	sprintf(str, poseMask, currentFrameNo);

	f = fopen(str, "r");
	if (f)
	{
		if (strcmp(inputFileType, "quaternion") == 0){

			Vector4f q;
			size_t ret = fscanf(f, "%f %f %f %f %f %f %f",
				&q[0], &q[1], &q[2], &q[3],
				&cached_pose->G.m30, &cached_pose->G.m31, &cached_pose->G.m32);
			
			cached_pose->G.setRot(rotationFromQuaternion(q));

			fclose(f);

			if (ret == 7) success = true;
		}
		else {
			size_t ret = fscanf(f, "%f %f %f %f %f %f %f %f %f %f %f %f",
				&cached_pose->G.m00, &cached_pose->G.m01, &cached_pose->G.m02,
				&cached_pose->G.m10, &cached_pose->G.m11, &cached_pose->G.m12,
				&cached_pose->G.m20, &cached_pose->G.m21, &cached_pose->G.m22,
				&cached_pose->G.m30, &cached_pose->G.m31, &cached_pose->G.m32);
			fclose(f);

			if (ret == 9) success = true;
		}

	}

	if (!success) {
		delete cached_pose; cached_pose = NULL;
		printf("error reading file '%s'\n", str);
	}
}

bool SE3SourceEngine::hasMoreMeasurements(void)
{
	loadSE3IntoCache();

	return (cached_pose != NULL);
}

void SE3SourceEngine::getMeasurement(ITMSE3Measurement *pose)
{
	bool bUsedCache = false;
	
	if (cached_pose != NULL)
	{
		pose->G = cached_pose->G;
		delete cached_pose;
		cached_pose = NULL;
		bUsedCache = true;
	}

	if (!bUsedCache) this->loadSE3IntoCache();

	++currentFrameNo;
}
