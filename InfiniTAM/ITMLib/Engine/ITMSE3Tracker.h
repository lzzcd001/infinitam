// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../Utils/ITMLibDefines.h"

#include "../Objects/ITMSE3Measurement.h"

#include "../Engine/ITMTracker.h"
#include "../Engine/ITMLowLevelEngine.h"

using namespace ITMLib::Objects;

namespace ITMLib
{
	namespace Engine
	{
		class ITMSE3Tracker : public ITMTracker
		{
		public:
			void TrackCamera(ITMTrackingState *trackingState, const ITMView *view);

			ITMSE3Tracker();
			virtual ~ITMSE3Tracker(void);
		};
	}
}
