// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../Objects/ITMView.h"
#include "../Objects/ITMSE3Measurement.h"

namespace ITMLib
{
	namespace Objects
	{
		/** \brief
		    Represents a single "view", i.e. RGB and depth images along
		    with all intrinsic and relative calibration information
		*/
		class ITMViewSE3 : public ITMView
		{
		public:
			ITMSE3Measurement *se3;

			ITMViewSE3(const ITMRGBDCalib *calibration, Vector2i imgSize_rgb, Vector2i imgSize_d, bool useGPU)
			 : ITMView(calibration, imgSize_rgb, imgSize_d, useGPU)
			{
				se3 = new ITMSE3Measurement();
			}

			~ITMViewSE3(void) { delete se3; }

			// Suppress the default copy constructor and assignment operator
			ITMViewSE3(const ITMViewSE3&);
			ITMViewSE3& operator=(const ITMViewSE3&);
		};
	}
}

