// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../ITMLib/ITMLib.h"

namespace InfiniTAM
{
	namespace Engine
	{
		class SE3SourceEngine
		{
		private:
			static const int BUF_SIZE = 2048;
			char poseMask[BUF_SIZE];

			ITMSE3Measurement *cached_pose;

			void loadSE3IntoCache();
			int cachedFrameNo;
			int currentFrameNo;
			const char *inputFileType;

		public:
			SE3SourceEngine(const char *poseMask, const char *inputFileType);
			~SE3SourceEngine() { }

			bool hasMoreMeasurements(void);
			void getMeasurement(ITMSE3Measurement *pose);
		};
	}
}

