// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include <cstdlib>

#include "Engine/UIEngine.h"
#include "Engine/ImageSourceEngine.h"
#include "ITMLib/Engine/ITMMainEngine.h"


using namespace InfiniTAM::Engine;

class InfiniTAMLib{

public:
  // Constructor
  InfiniTAMLib(const char* calibFileName, const char* rgbMask, const char* depthMask, bool visualizeTSDF=false);

  // Process next frame
  Matrix4f processNextFrame();

  // Get image source engine
  ImageSourceEngine* getImageSource(){
    return imageSource_;
  }


  ITMMainEngine* getMainEngine(){
    return mainEngine_;
  }

  ITMLibSettings* getInternalSettings(){
    return internalSettings_;
  }


protected:
  ITMUChar4Image *inputRGBImage; ITMShortImage *inputRawDepthImage;
  ImageSourceEngine* imageSource_;
  ITMMainEngine *mainEngine_;
  ITMLibSettings *internalSettings_;

};



