// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "ITMSE3Tracker.h"
#include "../Objects/ITMViewSE3.h"

using namespace ITMLib::Engine;

ITMSE3Tracker::ITMSE3Tracker(){}
ITMSE3Tracker::~ITMSE3Tracker(void) {}

void ITMSE3Tracker::TrackCamera(ITMTrackingState *trackingState, const ITMView *view)
{
	trackingState->pose_d->SetInvM (((ITMViewSE3*) view)->se3->G);
	trackingState->pose_d->Coerce();
}