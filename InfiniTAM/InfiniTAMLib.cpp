// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include <cstdlib>
#include <InfiniTAMLib.h>
#include <GL/freeglut.h>

using namespace InfiniTAM::Engine;

////////////////////////////////////////////////////////////////////////////////
InfiniTAMLib::InfiniTAMLib(const char* calibFileName, const char* rgbMask, const char* depthMask, bool visualizeTSDF){

  imageSource_ = new ImageFileReader(calibFileName, rgbMask, depthMask);

  internalSettings_ = new ITMLibSettings();
  internalSettings_->trackerType = ITMLibSettings::TRACKER_ICP;

  mainEngine_ = new ITMMainEngine(internalSettings_, &imageSource_->calib, imageSource_->getRGBImageSize(), imageSource_->getDepthImageSize());

  // allocate memory
  bool allocateGPU = false;
  inputRGBImage = new ITMUChar4Image(imageSource_->getRGBImageSize(), true, allocateGPU);
  inputRawDepthImage = new ITMShortImage(imageSource_->getDepthImageSize(), true, allocateGPU);

//  if(visualizeTSDF){
//      int argc; char** argv;
//      UIEngine::Instance()->Initialise(argc, argv, imageSource_, NULL, NULL, mainEngine_, "./Files/Out", internalSettings_->deviceType);
//      glutCheckLoop();
//     // UIEngine::Instance()->Run();
////      UIEngine::Instance()->Shutdown();
//    }

}

////////////////////////////////////////////////////////////////////////////////
Matrix4f InfiniTAMLib::processNextFrame(){

  // process next frame
  imageSource_->getImages(inputRGBImage, inputRawDepthImage);


  // process
  ITMPose* pose = mainEngine_->PoseFromProcessFrame(inputRGBImage, inputRawDepthImage);
  Matrix4f poseMat = pose->GetInvM();

//  UIEngine::Instance()->processedFrameNo++;
//  UIEngine::Instance()->needsRefresh = true;

  return poseMat;
}

