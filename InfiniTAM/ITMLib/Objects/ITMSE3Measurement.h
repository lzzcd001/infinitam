// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../Utils/ITMLibDefines.h"

namespace ITMLib
{
	namespace Objects
	{
		class ITMSE3Measurement
		{
		public:
			Matrix4f G;

			ITMSE3Measurement()
			{
				G.setIdentity();
			}

			ITMSE3Measurement(const Matrix4f & G)
			{
				this->G = G;
			}

			void SetFrom(const ITMSE3Measurement *measurement)
			{
				this->G = measurement->G;
			}

			~ITMSE3Measurement(void) { }

			// Suppress the default copy constructor and assignment operator
			ITMSE3Measurement(const ITMSE3Measurement&);
			ITMSE3Measurement& operator=(const ITMSE3Measurement&);
		};
	}
}
