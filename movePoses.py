import os

def movePoses(origin_filepath, target_folder):
	if not os.path.exists(target_folder):
		raise "target folder not exists"
	with open(origin_filepath, 'r') as posefile:
		poses = map(lambda x: x.rstrip(), posefile.readlines())
		for i, pose in enumerate(poses):
			target_filepath = os.path.join(target_folder, "%05d.txt" % i)
			with open(target_filepath, 'w') as target_file:
				target_file.write(pose)

if __name__ == "__main__":
	movePoses('/home/tom/rgbd-scenes-v2/pc/02.pose', '/home/tom/rgbd-scenes-v2/imgs/scene_02/poses')





